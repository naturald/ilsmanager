<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" value="{{ $object ? $object->name : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">E-Mail</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" value="{{ $object ? $object->email : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="website" class="col-sm-4 col-form-label">Sito Web</label>
    <div class="col-sm-8">
        <input type="url" class="form-control" name="website" value="{{ $object ? $object->website : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="birth_date" class="col-sm-4 col-form-label">Scadenza</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="expiration" value="{{ $object ? $object->expiration : date('Y-m-d', strtotime('+1 years')) }}">
    </div>
</div>
<div class="form-group row">
    <label for="logo" class="col-sm-4 col-form-label">Logo</label>
    <div class="col-sm-8">
        <input type="file" name="logo"><br>
        <img src="{{ $object ? route('sponsor.logo', $object->id) : '' }}"><br>
        <small class="form-text text-muted">Immagine quadrata, 192x192px</small>
    </div>
</div>
