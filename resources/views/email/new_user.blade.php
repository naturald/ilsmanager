<p>
    Nuova richiesta di iscrizione:
</p>
<ul>
    <li>{{ $user->printable_name }} ({{ $user->username }})</li>
    <li>{{ $user->address_street }}, {{ $user->address_place }} ({{ $user->address_prov }})</li>
</ul>
<p>
    <a href="{{ route('user.edit', $user->id) }}">Vedi qui i dettagli.</a>
</p>
<p>
    Si raccomanda di verificare il versamento della quota di iscrizione ed approvare la registrazione.
</p>
