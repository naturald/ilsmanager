@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Configurazioni Generali</h3>

            <form method="POST" action="{{ route('config.update', 0) }}">
                @method('PUT')
                @csrf

                @foreach(App\Config::allConfig() as $identifier => $metadata)
                    @if(!isset($metadata->activation) || ($metadata->activation)())
                        <div class="form-group row">
                            <label for="{{ $identifier }}" class="col-sm-4 col-form-label">{{ $metadata->label }}</label>
                            <div class="col-sm-8">
                                @if($metadata->type == 'text')
                                    <input type="text" class="form-control" name="{{ $identifier }}" value="{{ App\Config::getConfig($identifier) }}">
                                @elseif($metadata->type == 'longtext')
                                    <textarea class="form-control" name="{{ $identifier }}" rows="5">{{ App\Config::getConfig($identifier) }}</textarea>
                                @elseif($metadata->type == 'email')
                                    <input type="email" class="form-control" name="{{ $identifier }}" value="{{ App\Config::getConfig($identifier) }}">
                                @elseif($metadata->type == 'boolean')
                                    <input type="checkbox" name="{{ $identifier }}" {{ App\Config::getConfig($identifier) == '1' ? 'checked' : '' }}>
                                @elseif($metadata->type == 'euro')
                                    <div class="input-group">
                                        <input type="number" class="form-control" value="{{ App\Config::getConfig($identifier) }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">€</span>
                                        </div>
                                    </div>
                                @elseif($metadata->type == 'percentage')
                                    <div class="input-group">
                                        <input type="number" class="form-control" value="{{ App\Config::getConfig($identifier) }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                @elseif($metadata->type == 'special')
                                    @if($identifier == 'custom_email_extras')
                                        <?php $mails = json_decode(App\Config::getConfig('custom_email_extras')) ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>E-Mail</th>
                                                    <th>Password (lasciare in bianco per non modificare)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($mails as $mail)
                                                    <tr>
                                                        <td><input type="text" class="form-control" name="custom_email_extras_email[]" value="{{ $mail[0] }}"></td>
                                                        <td><input type="password" class="form-control" name="custom_email_extras_password[]"></td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td><input type="text" class="form-control" name="custom_email_extras_email[]" value=""></td>
                                                    <td><input type="password" class="form-control" name="custom_email_extras_password[]"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endif
                @endforeach

                <br>
                <hr>
                <br>

                <h3>Account Contabili</h3>

                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-7">
                                Nome
                            </div>
                            <div class="col-md-1">
                                Archiviato
                            </div>
                            <div class="col-md-3">
                                Padre
                            </div>
                            <div class="col-md-1">
                                Elimina
                            </div>
                        </div>
                    </li>

                    @foreach(App\Account::whereNull('parent_id')->orderBy('name', 'asc')->get() as $account)
                        @include('account.edit', ['account' => $account, 'deep' => 0])
                    @endforeach

                    @include('account.edit', ['account' => null, 'deep' => 0])
                </ul>

                <br>
                <hr>
                <br>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
