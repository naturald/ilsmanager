<?php

$item_class = '';

if (!$ar) {
    $item_class = 'list-group-item-danger';
}
else {
    if ($ar->account_id == 0)
        $item_class = 'list-group-item-danger';
    else if (!$ar->exists)
        $item_class = 'list-group-item-warning';
}

?>

<li class="list-group-item {{ $item_class }}">
    <input type="hidden" name="movement[]" value="{{ $movement->id }}">
    <input type="hidden" name="account_row[]" value="{{ $ar && $ar->exists ? $ar->id : 'new' }}">

    <div class="row">
        <div class="col-md-3">
            <input class="form-control" type="number" name="amount[]" value="{{ $ar ? ($ar->amount_in ?: ($ar->amount_out * -1)) : 0 }}" step="0.01">
        </div>
        <div class="col-md-3">
            @include('account.select', ['select' => $ar ? $ar->account_id : 0])
        </div>
        <div class="col-md-3">
            @include('user.select', ['select' => $ar ? $ar->user_id : 0, 'status' => ['pending', 'active', 'suspended']])
        </div>
        <div class="col-md-3">
            @include('section.select', ['select' => $ar ? $ar->section_id : 0])
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <input class="form-control" type="text" name="notes[]" value="{{ $ar ? $ar->notes : '' }}" placeholder="Causale">
        </div>
        <div class="col-md-2">
            <button class="btn btn-danger delete-row">Elimina</button>
        </div>
    </div>
</li>
