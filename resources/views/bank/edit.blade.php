@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('bank.update', $object->id) }}">
                @method('PUT')
                @csrf

                @include('bank.form', ['object' => $object])

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-12 mt-3">
            <form method="POST" action="{{ route('rule.update', $object->id) }}">
                @csrf
                @method('PUT')

                <h5>Regole assegnazione</h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th width="30%">Espressione regolare</th>
                            <th width="30%">Account</th>
                            <th width="30%">Annotazione</th>
                            <th width="10%">Elimina</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($object->rules as $rule)
                            @include('rule.editblock', ['rule' => $rule])
                        @endforeach
                        @include('rule.editblock', ['rule' => null])
                    </tbody>
                </table>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-12 mt-3">
            <h5>Ultimi 50 movimenti</h5>
            <table class="table">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Valore</th>
                        <th>Account</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($object->movements()->orderBy('date', 'desc')->take(50)->get() as $movement)
                        <tr>
                            <td>{{ $movement->date }}</td>
                            <td>{{ $movement->printable_amount }}</td>
                            <td>{{ $movement->account ? $movement->account->name : '???' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
