# ILSManager

Questo è il codice sorgente del gestionale interno dell'associazione Italian Linux Society.

Puoi vederlo online qui:

https://ilsmanager.linux.it/

## Installazione
### Dipendenze

* PHP PDO

### Installazione

```
composer install
cp .env.example .env
vim .env
php artisan migrate
php artisan key:generate
php artisan db:seed
```

### Avvio

```
php artisan serve
```

## Accesso di test

```
user        |    psw

admin       |    admin
member      |    member
referent    |    referent
pending     |    pending
suspended   |    suspended
expelled    |    expelled
dropped     |    dropped
association |    association
```

## Mailhog

https://github.com/mailhog/MailHog

```
MAIL_DRIVER=smtp
MAIL_HOST=0.0.0.0
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```

## PayPal

Puoi configurare la lettura automatica da un conto PayPal modificando il file `.env` con:

```
PAYPAL_USERNAME=
PAYPAL_PASSWORD=
PAYPAL_SIGNATURE=
PAYPAL_BANKID=
```

Oltre alle informazioni dalle API PayPal, BANKID è l'ID nel database del conto PayPal.

### Contributori

* Daniele Rizzo
* giomba
* Daniele Scasciafratte
* Roberto Guido (autore e sviluppatore principale)
* Daniele Scasciafratte (miglioramenti vari)
* Valerio Bozzolan (miglioramenti vari)

### Licenza

Copyright (C) 2019, 2020, 2021 Italian Linux Society and contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
