<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Receipt;

class ReceiptController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Receipt',
            'view_folder' => 'receipt'
        ]);
    }

    protected function requestToObject($request, $object)
    {
        $object->date = $request->input('date');
        $object->movement_id = $request->input('movement_id');
        $object->header = $request->input('header');
        $object->causal = $request->input('causal');
        $object->stamp = $request->input('stamp', '');
        return $object;
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'date' => 'date|date_format:Y-m-d',
            'movement_id' => 'required|number|exists:movements,id',
            'header' => 'required',
            'causal' => 'required'
        ];

        return $ret;
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    public function show($id)
    {
        $receipt = Receipt::findOrFail($id);
        $this->authorize('view', $receipt);
        $path = $receipt->path;

        if (file_exists($path) == false) {
            $receipt->generate();
        }

        return response()->download($path);
    }
}
