<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Auth;
use Hash;
use Log;

use App\Section;
use App\User;
use App\Config;

class UserController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\User',
            'view_folder' => 'user'
        ]);
    }

    protected function query($query, $id = null)
    {
        if (is_null($id)) {
            $query->with('section');
        }

        return $query;
    }

    protected function requestToObject($request, $object)
    {
        $fields = ['name', 'surname', 'email', 'phone', 'website', 'taxcode', 'birth_place', 'birth_prov', 'birth_date', 'address_street', 'address_place', 'address_zip', 'address_prov', 'shipping_name', 'shipping_street', 'shipping_place', 'shipping_zip', 'shipping_prov', 'size'];
        $object = $this->fitObject($object, $fields, $request);

        $object->volunteer = $request->has('volunteer');
        $object->section_id = $request->input('section_id', 0);

        if ($request->user()->hasRole('admin')) {
            $fields = ['username', 'type', 'status', 'notes', 'request_at', 'approved_at', 'expelled_at'];
            $object = $this->fitObject($object, $fields, $request);

            $roles = $request->input('roles', []);
            $object->roles()->sync($roles);
        }

        if (empty($object->notes)) {
            $object->notes = '';
        }

        if (Config::getConfig('custom_email_aliases') == '1') {
            $email_type = $request->input('email_behaviour', null);
            if (!is_null($email_type)) {
                if ($email_type == 'alias') {
                    $object->setConfig('email_behaviour', 'alias');
                }
                else {
                    $object->setConfig('email_behaviour', 'inbox');
                    $new_email_password = $request->input('email_password', null);
                    if (!empty($new_email_password)) {
                        Log::debug('Cambiata password email utente ' . $object->username);
                        $object->setConfig('email_password', mailPasswordHash($new_email_password));
                    }
                }
            }
        }

        $password = $request->input('password');
        if (!empty($password)) {
            $object->password = Hash::make($password);
        }
        else {
            if ($object->exists == false) {
                $object->password = Hash::make(str_random(10));
            }
        }

        return $object;
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'address_street' => 'required|max:255',
            'address_place' => 'required|max:255',
            'address_zip' => 'required|max:255',
            'address_prov' => 'required|max:2',
        ];

        if (Auth::user()->hasRole('admin')) {
            if (is_null($object)) {
                $ret['username'] = 'required|unique:users|max:255';
            }
            else {
                $ret['username'] = [
                    'required',
                    Rule::unique('users')->ignore($object->id),
                ];
            }

            $ret = array_merge($ret, [
                'type' => 'required|in:regular,association',
                'taxcode' => 'required|max:255',
                'birth_place' => 'required|max:255',
                'birth_prov' => 'required|max:2',
                'birth_date' => 'required|max:255',
            ]);
        }

        return $ret;
    }

    protected function defaultSortingColumn()
    {
        return 'surname';
    }

    public function approve(Request $request)
    {
        $this->checkAuth();
        $approved = $request->input('approved', []);
        User::where('status', 'pending')->whereIn('id', $approved)->update(['status' => 'active', 'approved_at' => date('Y-m-d')]);
        return redirect()->route('user.index');
    }

    public function bypass(Request $request, $id)
    {
        if ($request->user()->hasRole('admin')) {
            Auth::logout();
            Auth::loginUsingId($id);
        }

        return redirect('/');
    }
}
