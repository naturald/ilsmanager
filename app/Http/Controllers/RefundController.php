<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Refund;

class RefundController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Refund',
            'view_folder' => 'refund'
        ]);
    }

    protected function query($query, $id = null)
    {
        $user = Auth::user();

        if ($user->hasRole('admin') == false) {
            $query->where('user_id', $user->id);
        }

        return $query;
    }

    protected function requestToObject($request, $object)
    {
        $object->date = $request->input('date');
        $object->amount = $request->input('amount');
        $object->section_id = $request->input('section_id', 0);
        $object->notes = $request->input('notes');

        if ($request->user()->hasRole('admin')) {
            $object->refunded = $request->has('refunded');
            $object->user_id = $request->input('user_id', 0);
        }
        else {
            $object->user_id = $request->user()->id;
        }

        return $object;
    }

    protected function defaultValidations($object)
    {
        return [
            'amount' => 'required|numeric',
            'notes' => 'required',
        ];
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function afterSaving($request, $object)
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');

            foreach($files as $f) {
                $object->attachFile($f);
            }
        }
    }

    public function download(Request $request)
    {
        return response()->download(storage_path('refunds/' . $request->input('file')));
    }
}
