<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Config;

class ApiController extends Controller
{
    private function flatString($string)
    {
        $string = iconv('UTF-8','ASCII//TRANSLIT', $string);
        $string = str_replace(' ', '.', $string);
        $string = preg_replace('/[^A-Za-z0-9\.]/', '', $string);
        $string = strtolower($string);
        return $string;
    }

    public function emails(Request $request)
    {
        $token = $request->input('token');
        if ($token !== env('API_TOKEN')) {
            abort(403);
        }

        \Debugbar::disable();

        header("Content-Type: application/txt");
        header("Content-Disposition: attachment; filename=\"soci\"");

        echo "# Nome Cognome nick password\n";
        echo "# Nome Cognome nick . indirizzo@di.destinazione\n";
        echo "# la password puo' essere sostituita da un . punto\n";
        echo "\n";

        $users = User::whereIn('status', ['active', 'pending'])->has('fees')->orderBy('username', 'asc')->get();
        $surnames = [];

        $usernames = [];
        foreach($users as $u) {
            $usernames[] = $u->username;
        }

        foreach($users as $u) {
            $type = $u->getConfig('email_behaviour');

            if (is_null($type)) {
                continue;
            }

            $row = [];

            /*
                Questo è per rendere univoci i cognomi, anche rispetto agli
                username (se qualche cognome corrisponde ad un nome, non è
                impossibile una collisione)
            */
            $actual_surname = self::flatString($u->surname);

            foreach($surnames as $s) {
                if ($s == $actual_surname) {
                    $actual_surname .= str_random(3);
                    break;
                }
            }

            foreach($usernames as $s) {
                if ($s == $actual_surname) {
                    $actual_surname .= str_random(3);
                    break;
                }
            }

            $surnames[] = $actual_surname;

            $row[] = self::flatString($u->name);
            $row[] = $actual_surname;
            $row[] = $u->username;

            if ($type == 'alias') {
                $row[] = '.';
                $row[] = $u->email;
            }
            else if ($type == 'inbox') {
                $mail_password = $u->getConfig('email_password');

                /*
                    Se per qualche motivo la password della inbox non è settata,
                    ne viene generata una random. Questo perché non possono
                    esserci password non definite
                */
                if (empty(trim($mail_password))) {
                    $row[] = mailPasswordHash(str_random(10));
                }
                else {
                    $row[] = $u->getConfig('email_password');
                }
            }

            echo join(' ', $row) . "\n";
        }

        $mails = json_decode(Config::getConfig('custom_email_extras'));
        foreach($mails as $m) {
            $row = [];
            $row[] = self::flatString('custom');
            $row[] = self::flatString($m[0]);
            $row[] = $m[0];
            $row[] = $m[1];
            echo join(' ', $row) . "\n";
        }
    }

    public function websites(Request $request)
    {
        $token = $request->input('token');
        if ($token !== env('API_TOKEN')) {
            abort(403);
        }

        \Debugbar::disable();

        header("Content-Type: application/txt");
        header("Content-Disposition: attachment; filename=\"websites.csv\"");

        $rows = [];
        $users = User::whereIn('status', ['active', 'pending'])->where('website', '!=', '')->orderBy('username', 'asc')->get();

        foreach($users as $u) {
            $row = [];
            $row[] = $u->printable_name;

            $web = $u->website;
            if (substr($web, 0, 4) != 'http') {
                $web = 'http://' . $web;
            }
            $row[] = $web;

            $rows[] = join(',', $row);
        }

        sort($rows);
        echo join("\n", $rows);
    }
}
