<?php
/**
    ILS Manager
    Copyright (C) 2021 Italian Linux Society

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use PayPalAPIMovementsReader;

use App\Movement;
use App\Bank;
use App\Account;
use App\AccountRow;
use App\Refund;
use App\Config;

class MovementController extends Controller
{
    public function index(Request $request)
    {
        $this->checkAuth();

        // force integer
        $year = (int) $request->input('year', date('Y'));
        if( $year < 1000 ) {
            abort( 500, "whaat" );
        }

        $objects = AccountRow::with('movement')->whereHas('movement', function($query) use ($year) {
            $query->where(DB::raw('YEAR(date)'), $year);
        })->orderBy('id', 'desc')->get()->sortByDesc(function($a) {
            return $a->movement->date;
        });

        return view('movement.index', compact('objects', 'year'));
    }

    /**
     * Parse and store the PayPal file with movements
     */
    public function store(Request $request)
    {
        $this->checkAuth();

	if( $request->input('store_from') === 'paypal_api' ) {

		// eventually upload via PayPal API
		$this->storeFromPayPalAPI( $request );

	} else {

		// load from file
		$this->storeFromFile( $request );
	}

        return redirect()->route('movement.review');
    }

    /**
     * Load movements from file
     */
    private function storeFromFile(Request $request)
    {
        DB::beginTransaction();
        $bank = Bank::find($request->input('bank_id'));

        // no file no party
        if(!$request->file('file')) {
            abort( 400, "Please pick a file" );
        }

        if ($request->file('file')->isValid()) {
            switch($bank->type) {
                case 'paypal':
                    $index = 0;
                    $paypal_fee = 0;
                    $file = fopen($request->file->path(), 'r');

                    while($c = fgetcsv($file)) {
                        $index++;

                        if ($index == 1) {
                            continue;
                        }

                        try {
                            if (Movement::where('identifier', trim($c[12]))->first() != null) {
                                continue;
                            }

                            $type = trim($c[4]);
                            if ($type == 'Payment Reversal' || $type == 'Payment Refund') {
                                $movement = Movement::where('bank_id', $bank->id)->where('identifier', trim($c[24]))->first();
                                if ($movement) {
                                    $movement->delete();
                                    continue;
                                }
                            }

                            $movement = new Movement();
                            $movement->amount = (float) $c[7];
                            $movement->bank_id = $bank->id;
                            $movement->date = implode("-", array_reverse(explode("/", $c[0])));
                            $movement->identifier = trim($c[12]);

                            /*
                                Attenzione: la formattazione di questa riga
                                viene usata in Movement::guessRows() per
                                indovinare il tipo di movimento, non alterare!
                            */
                            $movement->notes = sprintf('%s - %s - %s', trim($c[3]), $movement->amount > 0 ? $c[10] : $c[11], $c[15]);

                            if ($movement->alreadyTracked()) {
                                continue;
                            }

                            $paypal_fee += (float) $c[8];

                            $movement->save();
                        }
                        catch(\Exception $e) {
                            echo $e->getMessage() . '<br>';
                            print_r($c);
                            DB::rollBack();
                            exit();
                        }
                    }

                    if ($paypal_fee != 0) {
                        $movement = new Movement();
                        $movement->amount = $paypal_fee;
                        $movement->bank_id = $bank->id;
                        $movement->date = date('Y-m-d');
                        $movement->identifier = '';
                        $movement->notes = 'Commissioni PayPal';
                        $movement->save();

                        $ar = new AccountRow();
                        $ar->movement_id = $movement->id;
                        $ar->account_id = Account::where('bank_costs', true)->first()->id;
                        $ar->notes = 'Commissioni PayPal';
                        $ar->amount_in = $paypal_fee;
                        $ar->save();
                    }

                    break;

                case 'unicredit':
                    $header_skipped = false;
                    $file = fopen($request->file->path(), 'r');

                    while($c = fgetcsv($file, 0, ';')) {
                        try {
                            if (empty($c) || empty($c[0])) {
                                continue;
                            }

                            if ($c[0] == 'Data' && $c[1] == 'Valuta') {
                                $header_skipped = true;
                                continue;
                            }

                            if ($header_skipped == false) {
                                continue;
                            }

                            $movement = new Movement();
                            $movement->amount = (float) str_replace(',', '', $c[3]);
                            $movement->bank_id = $bank->id;
                            $movement->date = implode("-", array_reverse(explode("/", $c[0])));
                            $movement->identifier = '';
                            $movement->notes = $c[2];

                            if ($movement->alreadyTracked())
                                continue;

                            $movement->save();
                        }
                        catch(\Exception $e) {
                            echo $e->getMessage() . '<br>';
                            print_r($c);
                            DB::rollBack();
                            exit();
                        }
                    }

                    break;
            }
        }

        DB::commit();
    }

    /**
     * Load movements from PayPal API
     *
     * @author Valerio Bozzolan
     */
    private function storeFromPayPalAPI( Request $request )
    {

        // if something goes wrong this throws a cute exception
        $config = PayPalAPIMovementsReader::validatedConfig();

        // we have to know the exact PayPal bank ID
        $bank_id = $config['bankid'];

        $fee_sum = 0.0;

        $startDate = null;
        $latest = Movement::where('bank_id', $bank_id)->orderBy('id', 'desc')->first();
        if ($latest) {
            $startDate = \DateTime::createFromFormat('Y-m-d', $latest->date)->sub(new \DateInterval('P1D'));
        }

        DB::beginTransaction();

        PayPalAPIMovementsReader::process( [
            /**
             * @see PayPalAPIMovementsReader::process()
             */
            'startDate' => $startDate,

            'itemCallback' => function ($id, $date, $amount, $fee, $causal) use ($bank_id, $fee_sum) {
                $existing_movement = Movement::where( 'bank_id', $bank_id )->where( 'identifier', $id )->first();
                if ($existing_movement == null) {
                    $movement = new Movement();
                    $movement->amount = $amount;
                    $movement->bank_id = $bank_id;
                    $movement->identifier = $id;
                    $movement->date = $date->format( 'Y-m-d' );
                    $movement->notes = $causal;

                    // do not save twice
                    if( !$movement->alreadyTracked() ) {
                        $movement->save();

                        // remember fees
                        if( $fee ) {
                            $fee_sum += $fee;
                        }
                    }
                }
            },
        ] );

        // eventually process fees
        if( $fee_sum ) {
            $movement = new Movement();
            $movement->amount = $fee_sum;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = 'Commissioni PayPal';
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->account_id = Account::where('bank_costs', true)->first()->id;
            $ar->notes = 'Commissioni PayPal';
            $ar->amount_in = $fee_sum;
            $ar->save();
        }

        DB::commit();
    }

    public function edit($id)
    {
        $this->checkAuth();
        $movement = Movement::find($id);
        return view('movement.edit', compact('movement'));
    }

    public function update(Request $request, $id)
    {
        $this->checkAuth();

        DB::beginTransaction();

        if ($id == 0) {
            $fees_account = Config::feesAccount();
            $rows = [];
            $fee_years = [];

            $movements = $request->input('movement');
            $account_rows = $request->input('account_row');
            $accounts = $request->input('account');
            $amounts = $request->input('amount');
            $users = $request->input('user');
            $sections = $request->input('section');
            $notes = $request->input('notes');

            $to_skip = [];

            $remove = $request->input('remove', []);
            foreach($remove as $r) {
                $m = Movement::find($r);
                $m->delete();
            }

            foreach($movements as $index => $id) {
                if (in_array($id, $to_skip)) {
                    continue;
                }

                $m = Movement::find($id);
                if (is_null($m)) {
                    continue;
                }

                if ($amounts[$index] == 0 || $accounts[$index] == 0) {
                    continue;
                }

                if ($accounts[$index] == $fees_account->id && $users[$index] == 0) {
                    continue;
                }

                $account_row_id = $account_rows[$index];

                if ($account_row_id == 'new') {
                    /*
                        I pagamenti delle quote sono gestiti in modo speciale,
                        per accertarsi di salvare un AccountRow (da cui poi
                        verrà generato una Fee) per ogni quota versata
                    */
                    if ($accounts[$index] == $fees_account->id) {
                        $ars = $m->getFeePayments($users[$index]);

                        if (!isset($rows[$id])) {
                            $rows[$id] = [];
                        }

                        foreach($ars as $ar) {
                            $ar->save();
                            $rows[$id][] = $ar->id;
                        }

                        $to_skip[] = $m->id;

                        continue;
                    }
                    else {
                        $ar = new AccountRow();
                    }
                }
                else {
                    $ar = AccountRow::find($account_row_id);
                }

                $ar->movement_id = $id;
                $ar->account_id = $accounts[$index];
                $ar->user_id = $users[$index];
                $ar->section_id = $sections[$index];
                $ar->notes = $notes[$index] ?: '';

                $amount = (float)$amounts[$index];
                if ($amount > 0)
                    $ar->amount_in = abs($amount);
                else
                    $ar->amount_out = abs($amount);

                $ar->save();

                if (!isset($rows[$id]))
                    $rows[$id] = [];
                $rows[$id][] = $ar->id;
            }

            foreach($rows as $movement_id => $rows_id) {
                AccountRow::where('movement_id', $movement_id)->whereNotIn('id', $rows_id)->delete();

                $m = Movement::find($movement_id);
                $m->generateReceipt();
            }
        }

        DB::commit();
        return redirect()->route('movement.index');
    }

    public function review()
    {
        $this->checkAuth();
        $pendings = Movement::whereDoesntHave('account_rows')->orderBy('date', 'asc')->paginate(20);
        return view('movement.review', compact('pendings'));
    }

    public function attach(Request $request, $id)
    {
        $this->checkAuth();
        $movement = Movement::find($id);
        $movement->attachFile($request->file('file'));
        return redirect()->route('movement.edit', $id);
    }

    public function refund(Request $request, $id)
    {
        $this->checkAuth();

        $ids = $request->input('refund_id');
        $refund = null;

        DB::beginTransaction();

        foreach($ids as $refund_id) {
            $refund = Refund::find($refund_id);
            $refund->movement_id = $id;
            $refund->refunded = true;
            $refund->save();
        }

        if ($refund && $refund->section) {
            $movement = Movement::find($id);
            foreach($movement->account_rows as $ar) {
                $ar->section_id = $refund->section_id;
                $ar->save();
            }
        }

        DB::commit();
        return redirect()->route('movement.edit', $id);
    }

    public function download(Request $request)
    {
        $this->checkAuth();
        return response()->download(storage_path('accounting/' . $request->input('file')));
    }
}
