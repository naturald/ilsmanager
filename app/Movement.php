<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

use DB;

use App\AccountRow;
use App\Config;

class Movement extends Model
{
    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function account_rows()
    {
        return $this->hasMany('App\AccountRow');
    }

    public function receipt()
    {
        return $this->hasOne('App\Receipt');
    }

    public function refund()
    {
        return $this->hasMany('App\Refund');
    }

    public function getAttachmentsPathAttribute()
    {
        return storage_path('accounting/' . $this->id);
    }

    public function getAttachmentsAttribute()
    {
        $ret = new Collection();

        $path = $this->attachments_path;
        if (file_exists($path)) {
            $files = scandir($path);

            foreach($files as $f) {
                if ($f[0] == '.')
                    continue;

                $ret->push($f);
            }
        }

        return $ret;
    }

    public function alreadyTracked()
    {
        return (Movement::where('date', $this->date)->where('amount', $this->amount)->where('identifier', $this->identifier)->where('notes', $this->notes)->count() != 0);
    }

    public function attachFile($file)
    {
        $file->move($this->attachments_path, $file->getClientOriginalName());
    }

    public function getFeePayments($user)
    {
        $ret = new Collection();

        if (is_object($user) == false) {
            $user_id = $user;
            $user = User::find($user_id);
            if (is_null($user)) {
                Log::error('Errore nel riconoscimento utente di cui generare le quote di iscrizione: ' . $user_id);
                return $ret;
            }
        }

        $fees_account = Config::feesAccount();
        $donations_account = Account::where('donations', true)->first();

        $amount = $this->amount;
        $year = $user->firstMissingFeeYear();
        $guessed = true;

        while($amount >= $user->feeAmount()) {
            $ar = new AccountRow();
            $ar->movement_id = $this->id;
            $ar->account_id = $fees_account->id;
            $ar->amount_in = $user->feeAmount();
            $ar->user_id = $user->id;
            $ar->notes = AccountRow::createFeeNote($user, $year);
            $ret->push($ar);

            $amount -= $user->feeAmount();
            $year++;
        }

        if ($amount != 0) {
            $ar = new AccountRow();
            $ar->movement_id = $this->id;
            $ar->account_id = $donations_account->id;
            $ar->amount_in = $amount;
            $ar->notes = sprintf('Donazione da parte di %s', $user->printable_name);
            $ret->push($ar);
        }

        return $ret;
    }

    public function getAccountRowFromHistory($guess_by_note)
    {
        $prev_movement = Movement::where('notes', 'like', $guess_by_note)->has('account_rows')->orderBy('id', 'desc')->first();

        if ($prev_movement) {
            $prev_account_row = $prev_movement->account_rows()->first();
            if ($prev_account_row) {
                $ar = new AccountRow();
                $ar->movement_id = $this->id;
                $ar->account_id = $prev_account_row->account_id;
                $ar->user_id = $prev_account_row->user_id;
                $ar->section_id = $prev_account_row->section_id;
                $ar->notes = $prev_account_row->notes;

                if ($this->amount > 0)
                    $ar->amount_in = abs($this->amount);
                else
                    $ar->amount_out = abs($this->amount);

                return $ar;
            }
        }

        return null;
    }

    private function guessByRules()
    {
        foreach($this->bank->rules as $rule) {
            if (preg_match($rule->fixed_rule, $this->notes)) {
                $ar = new AccountRow();
                $ar->movement_id = $this->id;
                $ar->account_id = $rule->account_id;
                $ar->notes = $rule->notes;

                if ($this->amount > 0)
                    $ar->amount_in = abs($this->amount);
                else
                    $ar->amount_out = abs($this->amount);

                $ret = new Collection();
                $ret->push($ar);
                return $ret;
            }
        }

        return null;
    }

    public function guessRows()
    {
        $ret = $this->guessByRules();
        $guessed = false;

        if ($ret) {
            $guessed = true;
        }
        else {
            $ret = new Collection();

            if ($this->bank->type == 'paypal') {
                list($name, $email, $causal) = explode(' - ', $this->notes);
                $name = str_replace("'", "\'", $name);

                $user = User::where(DB::raw("CONCAT(name, ' ', surname)"), $name)->first();
                if (!is_null($user) && $this->amount >= $user->feeAmount()) {
                    $ret = $this->getFeePayments($user);
                    $guessed = true;
                }
                else {
                    $guess_note = sprintf('%s - %s - %%', $name, $email);
                    $ar = $this->getAccountRowFromHistory($guess_note);
                    if ($ar) {
                        $ret->push($ar);
                        $guessed = true;
                    }
                }
            }
            else if ($this->bank->type == 'unicredit') {
                $name = trim(preg_replace('/^BONIFICO A VOSTRO FAVORE BONIFICO SEPA DA ([A-Z ]*) PER .*$/', '\1', $this->notes));
                $user = User::where(DB::raw("CONCAT(surname, ' ', name)"), $name)->first();
                if (!is_null($user) && $this->amount >= $user->feeAmount()) {
                    $ret = $this->getFeePayments($user);
                    $guessed = true;
                }
                else {
                    $ar = $this->getAccountRowFromHistory($this->notes);
                    if ($ar) {
                        $ret->push($ar);
                        $guessed = true;
                    }
                }
            }
        }

        if ($guessed == false) {
            $ar = new AccountRow();

            if ($this->amount > 0)
                $ar->amount_in = abs($this->amount);
            else
                $ar->amount_out = abs($this->amount);

            $ret->push($ar);
        }

        return $ret;
    }

    public function generateReceipt()
    {
        $causals = [];
        $headers = [];

        if ($this->amount <= 0) {
            return;
        }

        foreach($this->account_rows as $ar) {
            if ($ar->user) {
                $headers[] = $ar->user->full_address;
            }

            $causals[] = trim($ar->notes);
        }

        $receipt = $this->receipt;

        if (is_null($receipt)) {
            $receipt = new Receipt();
            $receipt->movement_id = $this->id;
        }

        $receipt->date = date('Y-m-d');
        $receipt->header = join("\n", $headers);
        $receipt->causal = join("\n", $causals);
        $receipt->save();
    }
}
