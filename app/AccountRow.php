<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountRow extends Model
{
    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function getAmountAttribute()
    {
        return $this->amount_in - $this->amount_out;
    }

    /*
        Questa funzione genera una stringa che sia poi leggibile da
        getFeeYear(). Permette di identificare i movimenti che sono stati a
        priori identificati come pagamenti di quote di iscrizione, per poi
        appunto poterle registrare in quanto quote.
    */
    public static function createFeeNote($user, $year)
    {
        return sprintf('Saldo quota %s %s', $year, $user->printable_name);
    }

    public static function createDonationNote($name)
    {
        return sprintf('Donazione per %s', $name);
    }

    public function getFeeYear()
    {
        return preg_replace('/^Saldo quota ([0-9]{4}) .*$/', '\1', $this->notes);
    }

    public function getPrintableAmountAttribute()
    {
        if (!empty($this->amount_in) && $this->amount_in != 0) {
            return $this->amount_in;
        }
        else {
            return ($this->amount_out * -1);
        }
    }
}
