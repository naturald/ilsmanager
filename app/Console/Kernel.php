<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
            Il 10 gennaio viene effettuato il primo controllo, che innesca le
            mail di notifica.
            Il 10 febbraio gli utenti non ancora in regola coi pagamenti vengono
            sospesi.
            Il 10 marzo gli utenti sospesi sono espulsi.
        */
        $schedule->command('check:fees')->cron('0 10 10 1 *');
        $schedule->command('check:suspend')->cron('0 10 10 2 *');
        $schedule->command('check:expelled')->cron('0 10 10 3 *');

        $schedule->command('check:requests')->daily();

        /*
            Il primo gennaio viene aggiornato il bilancio delle sezioni locali
        */
        $schedule->command('section:balance')->cron('0 1 1 1 *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
