<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Hash;

use App\User;

class ResetPassword extends Command
{
    protected $signature = 'reset:password {nickname} {password}';
    protected $description = 'Per resettare la password di un utente';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $identifier = $this->argument('nickname');
        $password = $this->argument('password');

        $user = User::where('username', $identifier)->first();
        if(is_null($user)) {
            $user = User::where('email', $identifier)->first();
            if(is_null($user)) {
                $this->error('Nessun utente trovato');
                exit();
            }
        }

        $user->password = Hash::make($password);
        $user->save();

        $this->info('Nuova password salvata');
    }
}
