<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class CheckSuspend extends Command
{
    protected $signature = 'check:suspend';
    protected $description = 'Sospende i soci morosi da più di due anni';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::where('status', 'active')->get();
        $threeshold = date('Y', strtotime('-24 months'));

        foreach($users as $u) {
            $last_fee = $u->fees()->max('year');
            if ($last_fee < $threeshold) {
                $u->status = 'suspended';
                $u->save();
            }
        }
    }
}
