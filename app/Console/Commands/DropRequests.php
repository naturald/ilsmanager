<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class DropRequests extends Command
{
    protected $signature = 'check:requests';
    protected $description = 'Invalida le richieste pendenti da più di 60 giorni senza quota pagata';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $interval = date('Y-m-d', strtotime('-60 days'));
        User::where('status', 'pending')->where('request_at', '<', $interval)->whereDoesntHave('fees')->update(['status' => 'dropped']);
    }
}
