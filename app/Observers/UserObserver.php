<?php

namespace App\Observers;

use Mail;
use Log;

use App\User;
use App\Config;

class UserObserver
{
    private function expulsion(&$user)
    {
        $user->sendMail('App\Mail\UserExpelled');
        $user->expelled_at = date('Y-m-d');
        $user->username = str_random(10);
        $user->email = str_random(5) . '-' . $user->email;
        Log::info('Espulsione del socio ' . $user->printable_name);
    }

    public function updating(User $user)
    {
        $original = User::find($user->id);

        if ($original->status != $user->status) {
            if ($original->status == 'pending') {
                if ($user->status == 'active') {
                    $user->sendMail('App\Mail\UserApproved');
                    $user->approved_at = date('Y-m-d');
                    Log::info('Attivazione del socio ' . $user->printable_name);
                }
                else if ($user->status == 'expelled') {
                    $user->sendMail('App\Mail\UserDeclined');
                    $user->expelled_at = date('Y-m-d');
                    $user->username = str_random(10);
                    $user->email = str_random(5) . '-' . $user->email;
                    Log::info('Espulsione del socio ' . $user->printable_name);
                }
            }
            else if ($original->status == 'active') {
                if ($user->status == 'suspended') {
                    $user->sendMail('App\Mail\UserSuspended');
                    Log::info('Sospensione del socio ' . $user->printable_name);
                }
                else if ($user->status == 'expelled') {
                    $this->expulsion($user);
                }
            }
            else if ($original->status == 'suspended') {
                if ($user->status == 'expelled') {
                    $this->expulsion($user);
                }
            }
        }

        if ($original->section_id != $user->section_id) {
            $fee = $user->fees()->where('year', date('Y'))->first();

            if ($fee) {
                $account_row = $fee->account_row;
                if ($account_row) {
                    if ($original->section_id == 0) {
                        $user->section->alterBalance(Config::getSectionQuote($account_row->amount_in), $account_row, $account_row->notes);
                    }
                    else {
                        $original->section->alterBalance(Config::getSectionQuote($account_row->amount_in) * -1, $account_row, '[Annullato]' . $account_row->notes);
                    }
                }
            }
        }

        return true;
    }
}
