<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    public function attachFile($file)
    {
        $path = storage_path('sponsors/');
        $file->move($path, $this->id);
    }
}
